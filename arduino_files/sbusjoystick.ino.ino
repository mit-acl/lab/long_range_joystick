#include <Joystick.h>
#include <SBUS.h>

#define SBUS_MIN 180
#define SBUS_MAX 1800

#define JS_MIN -127
#define JS_MAX 128

//#define LED_PIN 13

SBUS sbus(Serial1);
Joystick_ Joystick;

void setup() {
  Serial.begin(115200);

  // Initialize Joystick Library
  sbus.begin(false);

  Joystick.setXAxisRange(JS_MIN, JS_MAX);
  Joystick.setYAxisRange(JS_MIN, JS_MAX);
  Joystick.setZAxisRange(JS_MIN, JS_MAX);
  Joystick.setThrottleRange(JS_MIN, JS_MAX);
  Joystick.setRudderRange(JS_MIN, JS_MAX);
  
  Joystick.begin(false);

//  pinMode( LED_PIN, OUTPUT );
}

void loop() {

  sbus.process();

  int channels[16];

  for (int i=1; i<=16; i++) {
//    Serial.print(" CH" + String(i) + ": " + sbus.getChannel(i));
    channels[i]=map(sbus.getChannel(i), SBUS_MIN, SBUS_MAX, JS_MIN, JS_MAX);
//    Serial.print(" CH" + String(i) + ": " + channels[i]);
  }
//  Serial.println();

//  int LED_CHANNEL = 3;
//  int duty = map(sbus.getChannel(LED_CHANNEL), SBUS_MIN, SBUS_MAX, JS_MIN, JS_MAX);
//  analogWrite( LED_PIN, duty );

  Joystick.setXAxis(channels[4]);
  Joystick.setYAxis(-channels[3]);
//  Joystick.setZAxis(channels[4]);

  Joystick.setRxAxis(channels[2]);
  Joystick.setRyAxis(channels[1]);

  Joystick.setThrottle(channels[13]);
  Joystick.setRudder(channels[14]);

  Joystick.setSteering(channels[5]);
  Joystick.setBrake(channels[6]);

  for (int i=7; i<=12; i++) {
    if (channels[i] >= 0) {
      Joystick.pressButton(i-7);
    } else {
      Joystick.releaseButton(i-7);
    }
  }
  Joystick.sendState();
}
