Long-Range Joystick Setup
Lakshay Sharma, Michael Everett
6/2/22

# Objective

Most of the existing wireless joysticks used for robotics use a USB receiver that has pretty short range, Bluetooth which also has short range, or RC controllers that can't be directly plugged in and read on a robot's computer.
These are all inconvenient for field robotics in our experience.

Thus, this project developed a long-range joystick that can simply be plugged into the computer as if it were one of those easy-to-use USB joysticks.
That is, it will appear as a joystick device in Linux (e.g., as `/dev/js0`) and can be fed into typical ROS nodes to be converted into velocity commands.

To accomplish this objective, we first bought a 900MHz module for an FRSky RC transmitter.
We observed this signal can still be received in a nearby building, and over 100+ meters with line-of-sight, which is substantially better than other low-cost joysticks we had tried (e.g., PS4, XBox 360 and their knockoffs).
Then, we designed a simple PCB that plugs into an Arduino Micro, and we slightly modified some open-source code that can be flashed onto the Arduino to convert an RC receiver's SBUS signal into looking like a joystick device in Linux.
The PCB is probably overkill, since it just contains an inverter (which could likely be done in software) and some header pins, but it has been working for us.

# Bill of Materials

## Receiver Side
- [Arduino Micro w/ headers](https://smile.amazon.com/gp/product/B00AFY2S56/ref=ppx_yo_dt_b_asin_title_o00_s02?ie=UTF8&psc=1)
- [FRSky 900MHz Receiver](https://smile.amazon.com/gp/product/B087BXZ498/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1)
- PCB (We used PCBWay to fab it -- $5 for 5 boards + $24 shipping, arrived in 1-2 weeks)
- [2mm Right-Angle Pin Headers](https://smile.amazon.com/gp/product/B098KHHZ8Q/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1)
- 1x 0603 10k resistor
- 1x 0603 1k resistor
- 1x 2N3904-B331 transistor (we bought [this pack](https://smile.amazon.com/gp/product/B088DMF2YZ/ref=ppx_yo_dt_b_asin_title_o03_s00?ie=UTF8&psc=1))
- 2x 1x17 0.1" pitch female headers

## Transmitter Side
- [FRSky Transmitter](https://smile.amazon.com/gp/product/B07QJ38Q1X/ref=ppx_yo_dt_b_asin_title_o00_s02?ie=UTF8&psc=1)
- [FRSky 900MHz Module](https://smile.amazon.com/gp/product/B07VBLT2K6/ref=ppx_yo_dt_b_asin_title_o00_s03?ie=UTF8&psc=1)

# Assembly Instructions

- Get the PCB fabbed
- Solder the resistors, transistor, and header pins onto the board
- Solder the male header pins onto the receiver (headers come with the receiver)
- Push the receiver into the board (receiver's LED points upward, away from top of custom board)
- Push the Arduino into the board, using "USB" silkscreen as guide for orientation
- Plug the Arduino into the computer using a mini-USB cable

# Transmitter/Receiver Setup Instructions

- Pair the transmitter and receiver
- @Lakshay: Remap the axes? Or is that done in the arduino code? Anything else needed on the transmitter?

# Software Instructions

- Flash the hex file onto the Arduino
- Add the udev rule to your machine so that the joystick is symlinked to a consistent device name (will need to trigger a udev rule reset or unplug/replug the USB cable)

Now, you should be able to do `jstest /dev/joystick` on your computer and see that the axes/buttons' values change accordingly.


